package mysql

import (
	"database/sql"
	"log"
	"time"

	"github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/favadi/myprofile/storage"
)

// Storage is the implementation of storage.Interface interface using MySQL as
// database backend. The password hashing algorithm is bcrypt.
type Storage struct {
	db *sql.DB
}

// NewStorage creates a new instance of Storage.
func NewStorage(db *sql.DB) (*Storage, error) {
	const schema = `CREATE TABLE IF NOT EXISTS users
(
  id       INT AUTO_INCREMENT,
  email    VARCHAR(255) NOT NULL UNIQUE,
  password BINARY(60)   NULL,
  name     VARCHAR(255) NULL,
  address  VARCHAR(255) NULL,
  phone    VARCHAR(255) NULL,
  updated  timestamp    NULL,
  INDEX (email),
  PRIMARY KEY (id)
)`

	if _, err := db.Exec(schema); err != nil {
		return nil, err
	}

	return &Storage{db: db}, nil
}

func hashPassword(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

// CreateUser creates an user with given email, password. It will returns
// ErrExists if an user with same email is already exists.
func (s *Storage) CreateUser(email, password string) (int64, error) {
	hash, err := hashPassword(password)
	if err != nil {
		return 0, err
	}

	result, err := s.db.Exec(`INSERT INTO users(email, password, updated) VALUES (?, ?, ?)`,
		email,
		hash,
		time.Now().UTC(),
	)
	if err != nil {
		me, ok := err.(*mysql.MySQLError)
		if !ok {
			return 0, err
		}
		// an user with same email already exists
		// https://dev.mysql.com/doc/refman/8.0/en/server-error-reference.html#error_er_dup_entry
		if me.Number == 1062 {
			log.Printf("Storage.CreateUser: MySQL error number=%d message=%s", me.Number, me.Message)
			return 0, storage.ErrExists
		}
	}

	lastInsertId, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return lastInsertId, nil
}

// UpdateUser updates the users record with matching id with information in parameters.
func (s *Storage) UpdateUser(id int64, name, address, phone string) (err error) {
	result, err := s.db.Exec(`UPDATE users
SET name    = ?,
    address = ?,
    phone   = ?,
    updated = ?
WHERE id = ?`,
		name,
		address,
		phone,
		time.Now().UTC(),
		id,
	)

	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return storage.ErrNotExists
	}

	return nil
}

// GetUser returns the information of the database record with given id.
func (s *Storage) GetUser(id int64) (email, name, address, phone string, err error) {
	var (
		storedName    sql.NullString
		storedAddress sql.NullString
		storedPhone   sql.NullString
	)
	row := s.db.QueryRow(`SELECT email, name, address, phone FROM users WHERE id = ?`, id)
	if err = row.Scan(&email, &storedName, &storedAddress, &storedPhone); err == sql.ErrNoRows {
		err = storage.ErrNotExists
	}

	if storedName.Valid {
		name = storedName.String
	}

	if storedAddress.Valid {
		address = storedAddress.String
	}

	if storedPhone.Valid {
		phone = storedPhone.String
	}

	return
}

// Authenticate gets the stored hash and compare with given password.
func (s *Storage) Authenticate(email, password string) (id int64, err error) {
	var hash []byte

	row := s.db.QueryRow(`SELECT id, password FROM users WHERE email = ?`, email)
	if err := row.Scan(&id, &hash); err == sql.ErrNoRows {
		return 0, storage.ErrNotExists
	} else if err != nil {
		return 0, err
	}

	if err := bcrypt.CompareHashAndPassword(hash, []byte(password)); err != nil {
		log.Printf("failed to authenticate: email=%s err=%s", email, err.Error())
		return 0, storage.ErrUnauthenticated
	}
	return id, nil
}

// AuthenticateOAuth2User implements authentication logic for OAuth2 user.
func (s *Storage) AuthenticateOAuth2User(email, name string) (id int64, err error) {
	tx, err := s.db.Begin()
	if err != nil {
		return 0, err
	}
	defer commitOrRollback(tx, &err)

	var (
		storedID   int64
		storedName sql.NullString
	)
	row := tx.QueryRow(`SELECT id, name FROM users WHERE email = ?`, email)
	if err := row.Scan(&storedID, &storedName); err == sql.ErrNoRows {
		log.Printf("no user with email=%s exists in database", email)
		err = nil
		var result sql.Result
		result, err = tx.Exec(`INSERT INTO users(email, name, updated)
VALUES (?, ?, ?)`, email, name, time.Now().UTC())
		if err != nil {
			return 0, err
		}
		lastInsertedId, err := result.LastInsertId()
		if err != nil {
			return 0, err
		}
		return lastInsertedId, nil
	} else if err != nil {
		return 0, err
	}

	if !storedName.Valid || name != storedName.String {
		_, err = tx.Exec(`UPDATE users SET name = ?, updated = ? WHERE  id = ?`,
			name,
			time.Now().UTC(),
			storedID)
		if err != nil {
			return 0, err
		}
	}

	return storedID, nil
}

// ChangePassword of user with given id.
func (s *Storage) ChangePassword(id int64, currentPassword, newPassword string) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	defer commitOrRollback(tx, &err)

	var storedPassword []byte
	row := tx.QueryRow(`SELECT password FROM users WHERE id = ?`, id)
	if err := row.Scan(&storedPassword); err == sql.ErrNoRows {
		return storage.ErrNotExists
	} else if err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword(storedPassword, []byte(currentPassword)); err != nil {
		log.Printf("password does not match: err=%s", err.Error())
		return storage.ErrUnauthenticated
	}

	hashNewPassword, err := hashPassword(newPassword)
	if err != nil {
		return err
	}

	_, err = tx.Exec(`UPDATE users SET password = ?, updated = ? WHERE id = ?`,
		hashNewPassword,
		time.Now(),
		id,
	)
	return err
}
