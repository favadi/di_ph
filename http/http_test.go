package http

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql" // import MySQL driver
	"github.com/gorilla/sessions"

	"bitbucket.org/favadi/myprofile/storage/mysql"
)

const (
	devDB                = "myprofile_api_http_test"
	devUser              = "root"
	devPassword          = "myprofile_dev_password"
	devCookieStoreSecret = "myprofile_dev_cookie_store_secret"
)

var (
	db *sql.DB
	s  *Server
)

func TestCreateUser(t *testing.T) {
	var tests = []HTTPTestCase{
		{
			Msg:      "missing body",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Assert:   assertCode(http.StatusBadRequest),
		},
		{
			Msg:      "missing email",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "password": "xyz"
}`),
			Assert: assertCode(http.StatusBadRequest),
		},
		{
			Msg:      "missing password",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "email": "test1@example.com"
}`),
			Assert: assertCode(http.StatusBadRequest),
		},
		{
			Msg:      "invalid email",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "email": "invalid-email",
  "password": "123456789"
}`),
			Assert: assertCode(http.StatusBadRequest),
		},
		{
			Msg:      "invalid password",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "email": "test2@example.com",
  "password": "1234567"
}`),
			Assert: assertCode(http.StatusBadRequest),
		},
		{
			Msg:      "valid input",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "email": "test3@example.com",
  "password": "123456789"
}`),
			Assert: assertCode(http.StatusCreated),
		},
		{
			Msg:      "duplicated email",
			Endpoint: "/apis/users",
			Method:   http.MethodPost,
			Body: []byte(`{
  "email": "test3@example.com",
  "password": "12345678910"
}`),
			Assert: assertCode(http.StatusBadRequest),
		},
	}
	for _, tc := range tests {
		t.Run(tc.Msg, func(t *testing.T) { RunHTTPTestCase(t, tc, s.r) })
	}
}

func TestUpdateUser(t *testing.T) {
	createdID, err := s.st.CreateUser("charlie@example.com", "12345678910")
	if err != nil {
		t.Fatal(err)
	}

	validCookie, err := newTestCookie(createdID, s.store.(*sessions.CookieStore))
	if err != nil {
		t.Fatal(err)
	}

	invalidCookie := &http.Cookie{Name: "cookie_name", Value: "cookie_value"}

	var tests = []HTTPTestCase{
		{
			Msg:      "update without cookie",
			Endpoint: "/apis/users",
			Method:   http.MethodPut,
			Body: []byte(`{
  "name": "Charlie",
  "address": "Brazil",
  "phone": "999"
}`),
			Assert: assertCode(http.StatusForbidden),
		},
		{
			Msg:      "update with invalid cookie",
			Endpoint: "/apis/users",
			Method:   http.MethodPut,
			Cookie:   invalidCookie,
			Body: []byte(`{
  "name": "Charlie",
  "address": "Brazil",
  "phone": "999"
}`),
			Assert: assertCode(http.StatusForbidden),
		},
		{
			Msg:      "update with valid cookie",
			Endpoint: "/apis/users",
			Cookie:   validCookie,
			Method:   http.MethodPut,
			Body: []byte(`{
  "name": "Charlie",
  "address": "Brazil",
  "phone": "999"
}`),
			Assert: func(t *testing.T, rsp *httptest.ResponseRecorder) {
				if rsp.Code != http.StatusNoContent {
					t.Errorf("wrong return code, expected: %d, got %d", http.StatusNoContent, rsp.Code)
				}
				var (
					hash    []byte
					name    sql.NullString
					address sql.NullString
					phone   sql.NullString
				)
				row := db.QueryRow(`SELECT password, name, address, phone FROM users WHERE id = ?`, createdID)
				if err = row.Scan(&hash, &name, &address, &phone); err != nil {
					t.Fatal(err)
				}

				if !name.Valid {
					t.Fatalf("got NULL value for name after updated")
				}

				if name.String != "Charlie" {
					t.Fatalf("wrong name after updated: expected=%s got=%s", "Charlie", name.String)
				}

				if !address.Valid {
					t.Fatalf("got NULL value for address after updated")
				}

				if address.String != "Brazil" {
					t.Fatalf("wrong address after updated: expected=%s got=%s", "Brazil", address.String)
				}

				if !phone.Valid {
					t.Fatalf("got NULL value for phone after updated")
				}

				if phone.String != "999" {
					t.Fatalf("wrong phone after updated: expected=%s got=%s", "999", phone.String)
				}
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.Msg, func(t *testing.T) { RunHTTPTestCase(t, tc, s.r) })
	}
}

func TestGetUser(t *testing.T) {
	const (
		testEmail    = "mia@example.com"
		testPassword = "12345678910"
		testName     = "Mia"
		testAddress  = "mars"
		testPhone    = "2222"
	)

	createdID, err := s.st.CreateUser(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}

	if err = s.st.UpdateUser(createdID, testName, testAddress, testPhone); err != nil {
		t.Fatal(err)
	}

	validCookie, err := newTestCookie(createdID, s.store.(*sessions.CookieStore))
	if err != nil {
		t.Fatal(err)
	}

	invalidCookie := &http.Cookie{Name: "cookie_name", Value: "cookie_value"}

	var tests = []HTTPTestCase{
		{
			Msg:      "get without cookie",
			Endpoint: "/apis/users",
			Method:   http.MethodGet,
			Assert:   assertCode(http.StatusForbidden),
		},
		{
			Msg:      "get with invalid cookie",
			Endpoint: "/apis/users",
			Method:   http.MethodGet,
			Cookie:   invalidCookie,
			Assert:   assertCode(http.StatusForbidden),
		},
		{
			Msg:      "get valid user",
			Endpoint: "/apis/users",
			Method:   http.MethodGet,
			Cookie:   validCookie,
			Assert: func(t *testing.T, rsp *httptest.ResponseRecorder) {
				if rsp.Code != http.StatusOK {
					t.Errorf("wrong return code, expected: %d, got %d", http.StatusOK, rsp.Code)
				}

				var output = &getUserResponse{}
				if err = json.NewDecoder(rsp.Body).Decode(output); err != nil {
					t.Fatal(err)
				}
				if output.Email != testEmail {
					t.Fatalf("wrong email: expected=%s got=%s", testEmail, output.Email)
				}
				if output.Name != testName {
					t.Fatalf("wrong name: expected=%s got=%s", testName, output.Name)
				}
				if output.Address != testAddress {
					t.Fatalf("wrong address: expected=%s got=%s", testAddress, output.Address)
				}
				if output.Phone != testPhone {
					t.Fatalf("wrong phone: expected=%s got=%s", testPhone, output.Phone)
				}
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.Msg, func(t *testing.T) { RunHTTPTestCase(t, tc, s.r) })
	}
}

func TestMain(m *testing.M) {
	var err error
	db, err = sql.Open("mysql",
		fmt.Sprintf("%s:%s@/%s",
			devUser,
			devPassword,
			""))
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(fmt.Sprintf(`CREATE DATABASE IF NOT EXISTS %s`, devDB))
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Close(); err != nil {
		log.Fatal(err)
	}

	db, err = sql.Open("mysql",
		fmt.Sprintf("%s:%s@/%s",
			devUser,
			devPassword,
			devDB))
	if err != nil {
		log.Fatal(err)
	}

	st, err := mysql.NewStorage(db)
	if err != nil {
		log.Fatal(err)
	}

	s = NewServer(st, sessions.NewCookieStore([]byte(devCookieStoreSecret)))

	ret := m.Run()

	if _, err := db.Exec("DROP TABLE IF EXISTS users"); err != nil {
		log.Fatal(err)
	}

	os.Exit(ret)
}
