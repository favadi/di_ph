package storage

// Interface is common MyProfile API interface to interact with persistent storage service.
type Interface interface {
	// CreateUser stored a new user with given email, password to database, it returns
	// user id when succeed.
	// Implementation must refuse to store if the email is already exists.
	// The password must be encrypted before storing to database.
	CreateUser(email, password string) (int64, error)

	// UpdateUser updates user with given id information.
	UpdateUser(id int64, name, address, phone string) error

	// GetUser returns information of user with given id.
	GetUser(id int64) (email, name, address, phone string, err error)

	// Authenticate checks if given password is matched the hashed password stored in database. If password matches,
	// returns user id.
	Authenticate(email, password string) (id int64, err error)

	// AuthenticateOAuth2User creates new user for given OAuth2 user information.
	// If user already exists, it will update (if needed) and returns user id.
	AuthenticateOAuth2User(email, name string) (id int64, err error)

	// ChangePassword changes password of user with given id.
	ChangePassword(id int64, currentPassword, newPassword string) error
}
