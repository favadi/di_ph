package storage

import "errors"

var (
	// ErrExists is the error indicates that a resource already exists in database.
	ErrExists = errors.New("already exists")
	// ErrExists is the error indicates that a resource does not exists in database.
	ErrNotExists = errors.New("not exists")
	// ErrUnauthenticated is the error returned when user failed to authenticate.
	ErrUnauthenticated = errors.New("unauthenticated")
)
