package http

import (
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"

	"bitbucket.org/favadi/myprofile/storage"
)

const (
	authCookieName       = "auth"
	gAuthStateCookieName = "google-auth-state"
)

// Server is the HTTP server to handle MyProfile Rest APIs backend.
type Server struct {
	r          *mux.Router
	st         storage.Interface
	tmpl       *template.Template
	store      sessions.Store
	oauth2Conf *oauth2.Config
}

// NewServer creates a new instance of Server.
func NewServer(st storage.Interface, store sessions.Store, oauth2Conf *oauth2.Config) *Server {
	tmpl := template.Must(template.ParseGlob("../templates/*"))
	r := mux.NewRouter()
	s := &Server{
		r:          r,
		st:         st,
		tmpl:       tmpl,
		store:      store,
		oauth2Conf: oauth2Conf,
	}

	// frontend
	r.HandleFunc("/", s.index).Methods(http.MethodGet)
	r.HandleFunc("/register", s.register).Methods(http.MethodGet)
	r.HandleFunc("/login", s.login).Methods(http.MethodGet)
	r.HandleFunc("/profile", s.profile).Methods(http.MethodGet)
	r.HandleFunc("/google/login", s.googleLogin).Methods(http.MethodGet)
	r.HandleFunc("/google/callback", s.googleCallBack).Methods(http.MethodGet)

	// static files
	r.PathPrefix("/static").Handler(http.StripPrefix("/static", http.FileServer(http.Dir("../static"))))

	// APIs
	r.HandleFunc("/apis/users", s.createUser).Methods(http.MethodPost)
	r.HandleFunc("/apis/users/passwords", s.changePassword).Methods(http.MethodPost)
	r.HandleFunc("/apis/users", s.updateUser).Methods(http.MethodPut)
	r.HandleFunc("/apis/users", s.getUser).Methods(http.MethodGet)
	r.HandleFunc("/apis/sessions", s.createSession).Methods(http.MethodPost)
	r.HandleFunc("/apis/sessions", s.deleteSession).Methods(http.MethodDelete)

	return s
}

// Run starts the web server.
func (s *Server) Run(addr string) error {
	log.Printf("starting MyProfile server on %s", addr)
	handler := handlers.CombinedLoggingHandler(os.Stdout, s.r)
	return http.ListenAndServe(addr, handler)
}
