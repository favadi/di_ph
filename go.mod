module bitbucket.org/favadi/myprofile

require (
	github.com/asaskevich/govalidator v0.0.0-20180720115003-f9ffefc3facf // indirect
	github.com/go-ozzo/ozzo-validation v3.5.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/mux v1.7.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.1.3
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67
	golang.org/x/oauth2 v0.0.0-20190212230446-3e8b2be13635
)
