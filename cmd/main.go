package main

import (
	"database/sql"
	"flag"
	"log"

	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	"bitbucket.org/favadi/myprofile/http"
	stmysql "bitbucket.org/favadi/myprofile/storage/mysql"
)

func main() {
	var (
		addr                     string
		mysqlAddr                string
		mysqlUser                string
		mysqlPasswd              string
		mysqlDBName              string
		cookieStoreSecret        string
		googleSigninClientID     string
		googleSigninClientSecret string
		url                      string
	)

	flag.StringVar(&addr, "addr", "127.0.0.1:8000", "network address to listen to")
	flag.StringVar(&mysqlAddr, "mysql-addr", "127.0.0.1:3306", "MySQL server address")
	flag.StringVar(&mysqlUser, "mysql-user", "myprofile", "MySQL server user")
	flag.StringVar(&mysqlPasswd, "mysql-passwd", "myprofile", "MySQL server password")
	flag.StringVar(&mysqlDBName, "mysql-db", "myprofile", "MySQL server database name")
	flag.StringVar(&cookieStoreSecret, "cookie-store-secret", "", "cookie store secret key")
	flag.StringVar(&googleSigninClientID, "google-signin-client-id", "", "Google Signin Client ID")
	flag.StringVar(&googleSigninClientSecret, "google-signin-client-secret", "", "Google Signin Client Secret")
	flag.StringVar(&url, "url", "http://localhost:8000", "URL of this application")

	flag.Parse()

	if len(cookieStoreSecret) == 0 {
		log.Fatalf("missing cookie store key")
	}

	if len(googleSigninClientID) == 0 {
		log.Fatalf("missing google signin client id")
	}

	if len(googleSigninClientSecret) == 0 {
		log.Fatalf("missing google signin client secret")
	}

	oauth2Conf := &oauth2.Config{
		ClientID:     googleSigninClientID,
		ClientSecret: googleSigninClientSecret,
		RedirectURL:  url + "/google/callback",
		Scopes:       []string{"email", "profile"},
		Endpoint:     google.Endpoint,
	}

	store := sessions.NewCookieStore([]byte(cookieStoreSecret))

	mysqlConfig := mysql.NewConfig()
	mysqlConfig.Net = "tcp"
	mysqlConfig.Addr = mysqlAddr
	mysqlConfig.User = mysqlUser
	mysqlConfig.Passwd = mysqlPasswd
	mysqlConfig.DBName = mysqlDBName

	db, err := sql.Open("mysql", mysqlConfig.FormatDSN())
	if err != nil {
		log.Fatal(err)
	}

	st, err := stmysql.NewStorage(db)
	if err != nil {
		log.Fatal(err)
	}

	if err := http.NewServer(st, store, oauth2Conf).Run(addr); err != nil {
		log.Fatal(err)
	}
}
