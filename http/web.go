package http

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
)

func (s *Server) index(w http.ResponseWriter, r *http.Request) {
	session, err := s.store.Get(r, authCookieName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if session.IsNew {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	} else {
		http.Redirect(w, r, "/profile", http.StatusFound)
		return
	}
}

func (s *Server) profile(w http.ResponseWriter, r *http.Request) {
	if err := s.tmpl.ExecuteTemplate(w, "Profile", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) login(w http.ResponseWriter, r *http.Request) {
	if err := s.tmpl.ExecuteTemplate(w, "Login", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *Server) register(w http.ResponseWriter, r *http.Request) {
	if err := s.tmpl.ExecuteTemplate(w, "Register", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func generateState() (string, error) {
	var buf = make([]byte, 64)
	if _, err := rand.Read(buf); err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(buf), nil
}

func (s *Server) googleLogin(w http.ResponseWriter, r *http.Request) {
	// generate a random state, store to cookie
	state, err := generateState()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session, err := s.store.Get(r, gAuthStateCookieName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	session.Options.MaxAge = 5 * 60 // 5 minutes
	session.Values["state"] = state
	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, s.oauth2Conf.AuthCodeURL(state), http.StatusFound)
}

// userProfile is the information returned by Google Sign In
type userProfile struct {
	Sub   string `json:"sub"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func (s *Server) googleCallBack(w http.ResponseWriter, r *http.Request) {
	var (
		inputState = r.FormValue("state")
		inputCode  = r.FormValue("code")
	)

	// get and validate the state value from cookie
	session, err := s.store.Get(r, gAuthStateCookieName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	stateVal, ok := session.Values["state"]
	if !ok {
		http.Error(w, err.Error(), http.StatusForbidden)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// remove state session to avoid reusing
	session.Options.MaxAge = -1
	if err = session.Save(r, w); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	state, ok := stateVal.(string)
	if !ok {
		http.Error(w, "malformed state data", http.StatusForbidden)
		return
	}

	if state != inputState {
		http.Error(w, "state does not match", http.StatusForbidden)
		return
	}

	token, err := s.oauth2Conf.Exchange(context.Background(), inputCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	// using obtained token to request for user information
	client := s.oauth2Conf.Client(context.Background(), token)
	rsp, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	defer rsp.Body.Close()

	var profile = &userProfile{}
	if err = json.NewDecoder(rsp.Body).Decode(profile); err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	id, err := s.st.AuthenticateOAuth2User(profile.Email, profile.Name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err = storeAuthSession(s.store, w, r, id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/profile", http.StatusFound)
}
