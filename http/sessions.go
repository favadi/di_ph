package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/gorilla/sessions"

	"bitbucket.org/favadi/myprofile/storage"
)

type createSessionRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func storeAuthSession(store sessions.Store, w http.ResponseWriter, r *http.Request, id int64) error {
	session, err := store.New(r, authCookieName)
	if err != nil {
		return err
	}

	session.Options.HttpOnly = true
	session.Values["id"] = id
	return session.Save(r, w)
}

func (r *createSessionRequest) validate() error {
	return validation.ValidateStruct(r,
		validation.Field(
			&r.Email,
			validation.Required,
			is.Email,
		),
		validation.Field(
			&r.Password,
			validation.Required,
			validation.Length(minPasswordLength, 0),
		),
	)
}

func (s *Server) createSession(w http.ResponseWriter, r *http.Request) {
	var input = &createSessionRequest{}
	if err := json.NewDecoder(r.Body).Decode(input); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err := input.validate(); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	id, err := s.st.Authenticate(input.Email, input.Password)
	if err == storage.ErrNotExists {
		responseJSON(w, newErrorResponse(err), http.StatusNotFound)
		return
	} else if err == storage.ErrUnauthenticated {
		responseJSON(w, newErrorResponse(err), http.StatusForbidden)
		return
	} else if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}

	if err = storeAuthSession(s.store, w, r, id); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (s *Server) authenticateSession(r *http.Request) (int64, *sessions.Session, error) {
	session, err := s.store.Get(r, authCookieName)
	if err != nil {
		return 0, nil, err
	}
	if session.IsNew {
		return 0, nil, errors.New("missing auth session")
	}
	sid, ok := session.Values["id"]
	if !ok {
		return 0, nil, errors.New("malformed auth session")
	}

	id, ok := sid.(int64)
	if !ok {
		return 0, nil, errors.New("malformed data found in auth session")
	}
	return id, session, nil
}

func (s *Server) deleteSession(w http.ResponseWriter, r *http.Request) {
	_, session, err := s.authenticateSession(r)
	if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusForbidden)
		return
	}

	// deletes session
	session.Options.MaxAge = -1
	if err := session.Save(r, w); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
