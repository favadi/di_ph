package http

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
)

type assertFn func(t *testing.T, rsp *httptest.ResponseRecorder)

// HTTPTestCase struct for http test case
type HTTPTestCase struct {
	Msg      string
	Endpoint string
	Method   string
	Params   map[string]string
	Body     []byte
	Cookie   *http.Cookie
	Assert   assertFn
}

// RunHTTPTestCase run http request test case
func RunHTTPTestCase(t *testing.T, tc HTTPTestCase, handler http.Handler) {
	t.Helper()
	req, err := http.NewRequest(tc.Method, tc.Endpoint, bytes.NewBuffer(tc.Body))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Add("Content-Type", "application/json")
	q := req.URL.Query()
	for k, v := range tc.Params {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()

	if tc.Cookie != nil {
		req.AddCookie(tc.Cookie)
	}

	rsp := httptest.NewRecorder()
	handler.ServeHTTP(rsp, req)
	tc.Assert(t, rsp)
}

func assertCode(code int) assertFn {
	return func(t *testing.T, rsp *httptest.ResponseRecorder) {
		t.Helper()
		if rsp.Code != code {
			t.Errorf("wrong return code, expected: %d, got %d", code, rsp.Code)
		}
	}
}

func newTestCookie(id int64, cs *sessions.CookieStore) (*http.Cookie, error) {
	session := sessions.NewSession(cs, authCookieName)
	session.Values["id"] = id

	encoded, err := securecookie.EncodeMulti(session.Name(), session.Values,
		s.store.(*sessions.CookieStore).Codecs...)
	if err != nil {
		return nil, err
	}

	cookie := sessions.NewCookie(session.Name(), encoded, session.Options)
	return cookie, nil
}
