package http

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"bitbucket.org/favadi/myprofile/storage"
)

const (
	minPasswordLength = 8
	maxInputLength    = 254
)

type createUserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type createUserResponse struct {
	ID int64 `json:"id"`
}

func (r *createUserRequest) validate() error {
	return validation.ValidateStruct(r,
		validation.Field(
			&r.Email,
			validation.Required,
			is.Email,
		),
		validation.Field(
			&r.Password,
			validation.Required,
			validation.Length(minPasswordLength, 0),
		),
	)
}

// createUser creates a new user object and save to database.
// This method will refuse to overrides a already exist user and returns an error.
func (s *Server) createUser(w http.ResponseWriter, r *http.Request) {
	var input = &createUserRequest{}
	if err := json.NewDecoder(r.Body).Decode(input); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err := input.validate(); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	lastInsertId, err := s.st.CreateUser(input.Email, input.Password)
	if err == storage.ErrExists {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	} else if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}

	responseJSON(w, createUserResponse{ID: lastInsertId}, http.StatusCreated)
}

type updateUserRequest struct {
	Name    string `json:"name"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
}

func (r *updateUserRequest) validate() error {
	return validation.ValidateStruct(r,
		validation.Field(
			&r.Name,
			validation.Length(0, maxInputLength),
		),
		validation.Field(
			&r.Address,
			validation.Length(0, maxInputLength),
		),
		validation.Field(
			&r.Phone,
			validation.Length(0, maxInputLength),
			// how to validate phone number?
		),
	)
}

// updateUser updates given user information to database.
func (s *Server) updateUser(w http.ResponseWriter, r *http.Request) {
	var (
		input = &updateUserRequest{}
	)

	id, _, err := s.authenticateSession(r)
	if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusForbidden)
		return
	}

	if err = json.NewDecoder(r.Body).Decode(input); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err = input.validate(); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err = s.st.UpdateUser(id, input.Name, input.Address, input.Phone); err == storage.ErrNotExists {
		responseJSON(w, newErrorResponse(err), http.StatusNotFound)
		return
	} else if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

type getUserResponse struct {
	Email   string `json:"email"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Phone   string `json:"phone"`
}

func (s *Server) getUser(w http.ResponseWriter, r *http.Request) {
	id, _, err := s.authenticateSession(r)
	if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusForbidden)
		return
	}

	email, name, address, phone, err := s.st.GetUser(int64(id))
	if err == storage.ErrNotExists {
		responseJSON(w, newErrorResponse(err), http.StatusNotFound)
		return
	} else if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	responseJSON(w,
		getUserResponse{
			Email:   email,
			Name:    name,
			Address: address,
			Phone:   phone,
		},
		http.StatusOK,
	)
}

type changePasswordRequest struct {
	CurrentPassword string `json:"current_password"`
	NewPassword     string `json:"new_password"`
}

func (r *changePasswordRequest) validate() error {
	return validation.ValidateStruct(r,
		validation.Field(
			&r.CurrentPassword,
			validation.Required,
			validation.Length(minPasswordLength, 0),
		),
		validation.Field(
			&r.NewPassword,
			validation.Required,
			validation.Length(minPasswordLength, 0),
		),
	)
}

func (s *Server) changePassword(w http.ResponseWriter, r *http.Request) {
	var (
		input = &changePasswordRequest{}
	)

	id, _, err := s.authenticateSession(r)
	if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusForbidden)
		return
	}

	if err = json.NewDecoder(r.Body).Decode(input); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err := input.validate(); err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusBadRequest)
		return
	}

	if err := s.st.ChangePassword(id, input.CurrentPassword, input.NewPassword); err == storage.ErrNotExists {
		responseJSON(w, newErrorResponse(err), http.StatusNotFound)
		return
	} else if err == storage.ErrUnauthenticated {
		responseJSON(w, newErrorResponse(errors.New("wrong password")), http.StatusForbidden)
		return
	} else if err != nil {
		responseJSON(w, newErrorResponse(err), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
