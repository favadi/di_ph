function login(data) {
    $.ajax({
        url: "/apis/sessions",
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        data: data,
        success: function (result) {
            $(location).attr("href", "/profile");
        },
        error: function (xhr, resp, text) {
            displayError("failed to login: " + xhr.responseJSON.error);
        }
    })
}

$(function () {
    $('a').each(function () {
        if ($(this).prop('href') === window.location.href) {
            $(this).addClass('current');
        }
    });
});

function displayError(msg) {
    var elem = $("#error");
    elem.text(msg);
    elem.show();
}

$(document).ready(function () {
    $("#logout").on("click", function () {
        $.ajax({
            url: "/apis/sessions",
            type: "DELETE",
            contentType: "application/json",
            dataType: "json",
            success: function (result) {
                console.log("invalidated session");
                $(location).attr("href", "/");
            },
            error: function (xhr, resp, text) {
                displayError("failed to logout: " + xhr.responseJSON.error);
            }
        });
        return false;
    })
});