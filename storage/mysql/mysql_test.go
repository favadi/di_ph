package mysql

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql" // import MySQL driver
	"golang.org/x/crypto/bcrypt"

	"bitbucket.org/favadi/myprofile/storage"
)

const (
	devDB       = "myprofile_api_storage_mysql_test"
	devUser     = "root"
	devPassword = "myprofile_dev_password"
)

var st *Storage

func TestCreateUser(t *testing.T) {
	const (
		testEmail    = "me@example.com"
		testPassword = "123456"
	)

	createdID, err := st.CreateUser(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}

	var (
		id             int64
		email          string
		hashedPassword []byte
	)
	row := st.db.QueryRow(`SELECT id,email,password
FROM users
WHERE email = ?`, testEmail)
	if err := row.Scan(&id, &email, &hashedPassword); err != nil {
		t.Fatal(err)
	}

	if id != createdID {
		t.Errorf("expected stored id=%d, got=%d", createdID, id)
	}

	if email != testEmail {
		t.Errorf("expected stored email=%s, got=%s", testEmail, email)
	}
	if len(hashedPassword) == 0 {
		t.Errorf("password is not stored in database")
	}

	if _, err := st.CreateUser(testEmail, testPassword); err != storage.ErrExists {
		t.Errorf("expected error=%s, got=%v", storage.ErrExists.Error(), err)
	}
}

func TestUpdateUser(t *testing.T) {
	const (
		testEmail    = "john@example.com"
		testPassword = "123456"
		testName     = "John Doe"
		testAddress  = "moon"
		testPhone    = "8888"
	)

	createdID, err := st.CreateUser(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}

	if err = st.UpdateUser(createdID, testName, testAddress, testPhone); err != nil {
		t.Fatal(err)
	}
	var (
		hash    []byte
		name    sql.NullString
		address sql.NullString
		phone   sql.NullString
	)
	row := st.db.QueryRow(`SELECT password, name, address, phone FROM users WHERE id = ?`, createdID)
	if err = row.Scan(&hash, &name, &address, &phone); err != nil {
		t.Fatal(err)
	}

	if err = bcrypt.CompareHashAndPassword(hash, []byte(testPassword)); err != nil {
		t.Fatalf("password is changed after update")
	}

	if !name.Valid {
		t.Fatalf("got NULL value for name after updated")
	}

	if name.String != testName {
		t.Fatalf("wrong name after updated: expected=%s got=%s", testName, name.String)
	}

	if !address.Valid {
		t.Fatalf("got NULL value for address after updated")
	}

	if address.String != testAddress {
		t.Fatalf("wrong address after updated: expected=%s got=%s", testAddress, address.String)
	}

	if !phone.Valid {
		t.Fatalf("got NULL value for phone after updated")
	}

	if phone.String != testPhone {
		t.Fatalf("wrong phone after updated: expected=%s got=%s", testPhone, phone.String)
	}
}

func TestGetUser(t *testing.T) {
	const (
		testEmail    = "alan@example.com"
		testPassword = "123456"
		testName     = "Alan"
		testAddress  = "sun"
		testPhone    = "1111"
	)

	createdID, err := st.CreateUser(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}

	if err = st.UpdateUser(createdID, testName, testAddress, testPhone); err != nil {
		t.Fatal(err)
	}

	email, name, address, phone, err := st.GetUser(createdID)
	if err != nil {
		t.Fatal(err)
	}
	if email != testEmail {
		t.Fatalf("wrong email: expected=%s got=%s", testEmail, email)
	}
	if name != testName {
		t.Fatalf("wrong name: expected=%s got=%s", testName, name)
	}
	if address != testAddress {
		t.Fatalf("wrong address: expected=%s got=%s", testAddress, address)
	}
	if phone != testPhone {
		t.Fatalf("wrong phone: expected=%s got=%s", testPhone, phone)
	}

	if _, _, _, _, err = st.GetUser(createdID + 1000); err != storage.ErrNotExists {
		t.Fatal(err)
	}
}

func TestAuthenticate(t *testing.T) {
	const (
		testEmail    = "daisy@example.com"
		testPassword = "123456"
	)

	createdID, err := st.CreateUser(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}

	id, err := st.Authenticate(testEmail, testPassword)
	if err != nil {
		t.Fatal(err)
	}
	if id != createdID {
		t.Error("failed to authenticate with correct password")
	}

	if _, err = st.Authenticate("daisyImpostor@example.com", testPassword); err != storage.ErrNotExists {
		t.Errorf("got wrong error for non existing user: expected=%s got=%s", storage.ErrNotExists.Error(), err.Error())
	}

	if id, err = st.Authenticate(testEmail, "wrong-password"); err == nil {
		t.Error("success to authenticate with wrong password")
	}
	if id != 0 {
		t.Error("id is returned for wrong password")
	}
}

func TestMain(m *testing.M) {
	db, err := sql.Open("mysql",
		fmt.Sprintf("%s:%s@/%s",
			devUser,
			devPassword,
			""))
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(fmt.Sprintf(`CREATE DATABASE IF NOT EXISTS %s`, devDB))
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Close(); err != nil {
		log.Fatal(err)
	}

	db, err = sql.Open("mysql",
		fmt.Sprintf("%s:%s@/%s",
			devUser,
			devPassword,
			devDB))
	if err != nil {
		log.Fatal(err)
	}

	st, err = NewStorage(db)
	if err != nil {
		log.Fatal(err)
	}

	ret := m.Run()

	if _, err := db.Exec("DROP TABLE IF EXISTS users"); err != nil {
		log.Fatal(err)
	}

	os.Exit(ret)
}
