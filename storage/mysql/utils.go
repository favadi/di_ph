package mysql

import (
	"database/sql"
	"fmt"
	"log"
)

func commitOrRollback(tx *sql.Tx, err *error) {
	if *err == nil {
		if cErr := tx.Commit(); cErr != nil {
			*err = fmt.Errorf("failed to commit transaction: %v", cErr)
		}
		return
	}

	if rErr := tx.Rollback(); rErr != nil {
		log.Print("failed to rollback transaction", "error", rErr)
	}
}
