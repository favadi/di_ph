FROM golang:1.11-stretch AS build-env

COPY . /myprofile
WORKDIR /myprofile/cmd
RUN go build -v -o myprofile-cli

FROM debian:stretch
COPY --from=build-env /myprofile/cmd/myprofile-cli /myprofile/cmd/myprofile-cli
COPY --from=build-env /myprofile/templates /myprofile/templates
COPY --from=build-env /myprofile/static /myprofile/static

RUN apt-get update && \
    apt-get install -y ca-certificates && \
    rm -rf /var/lib/apt/lists/*

ENV HTTP_ADDRESS=0.0.0.0:8000
EXPOSE 8000
WORKDIR /myprofile/cmd
ENTRYPOINT ["/myprofile/cmd/myprofile-cli"]
