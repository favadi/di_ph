package http

import (
	"encoding/json"
	"net/http"
)

func setContentTypeJSON(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
}

type errorResponse struct {
	Error string `json:"error"`
}

func newErrorResponse(err error) *errorResponse {
	return &errorResponse{Error: err.Error()}
}

func responseJSON(w http.ResponseWriter, rsp interface{}, code int) {
	setContentTypeJSON(w)
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(rsp); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
