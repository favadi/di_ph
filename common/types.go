package common

// GoogleSigninClient contains id and secret, will be used for register/login with Google.
type GoogleSigninClient struct {
	ID     string `json:"id"`
	Secret string `json:"secret"`
}
